# European Language Grid Python SDK

The [**European Language Grid**](https://live.european-language-grid.eu/) is the primary platform for Language Technology in Europe. With the ELG Python SDK, you can use LT services and search the catalog inside your Python projects.

To have more information about the Python SDK, please look at the documentation: [https://european-language-grid.readthedocs.io/en/release1.1.2/all/A1_PythonSDK/PythonSDK.html](https://european-language-grid.readthedocs.io/en/release1.1.2/all/A1_PythonSDK/PythonSDK.html).